# LCD Temperature Display
This project uses a AVR ATMega328P to read a external temperature sensor and display the temperature on a I2C 2x16 LCD display.

Note: The PCF8574 chip broke during development, as result this is untested and still a working progress.

# Hardware
Microcontroller: ATMega328P
Temperature Sensor: TMP35
LCD Display: HD44780 Controlled Display
I2C Chip: PCF8574 to control datapin on HD44780 display

# Build Software
The induced make file depends on AVR-GCC and avrdude in the path of the build machine.

# Circuit Setup
The TMP35 Vout is connected to ADC0 on the ATMega328P.
The only I2C bus on ATMega328P is connected to the PCF8574 chip. 
P0 (IC pin 4) on PCF8574 is connected to RS on the display.
P1 (IC pin 5) on PCF8574 is connected to RW on the display.
P2 (IC pin 6) on PCF8574 is connected to E on the display.
P4 (IC pin 8) on PCF8574 is connected to D4 on the display.
P5 (IC pin 9) on PCF8574 is connected to D5 on the display.
P6 (IC pin 10) on PCF8574 is connected to D6 on the display.
P7 (IC pin 11) on PCF8574 is connected to D7 on the display.
