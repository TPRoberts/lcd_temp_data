/*
I2C Dirver Library Source file
Created by: Thomas Roberts
*/
#define F_CPU 16000000UL
#include <util/twi.h>
#include "i2c.h"

// Define the I2C clock in Hz
#define SCL_CLOCK 100000L

/*
Function: i2c_init
Description: This function will initialise the I2C.
Parameters: None
Returns: None
Notes: Should only be called once
*/
void i2c_init (void)
{
    // Set the TWI bit in Power Reduction Register to 0 (Should be this from default but just encase)
    PRR &= (uint8_t)~(1 << PRTWI);

    // initialise the Status Register to 0
    TWSR = 0;
    // Calculate the Bit rate using CPU frequency and SCL clock, formular from ATMega Spec
    TWBR = ((F_CPU/SCL_CLOCK)-16)/2;
}
/* End i2c_init */

/*
Function: i2c_start
Description: This function will issue a start condition,
Send device address and transfer direction.
Parameters: Address of the slave device
Returns: Boolean (True for success)
*/
bool i2c_start (const uint8_t address)
{
    uint8_t status = 0;        // Used to check the status register
    bool ReturnValue = false;  // Return Value initialised to false

    // Send the start condition to the slave
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

    // Wait for TWINT flag to be set. This will indicate the start condition has been transmitted
    while (!(TWCR & (1 << TWINT)));

    // Check the value of of the TWI status register
    // This 0xF8 is used to mask the prescaler bits
    status = TW_STATUS & 0xF8;
    if ((status == TW_START) || (status == TW_REP_START))
    {
        // Send the device salve address
        TWDR = address;
        TWCR = (1 << TWINT) | (1 << TWEN);

        // Wait for TWINT flag to be set
        while (!(TWCR & (1 << TWINT)));

        // Check the status register
        status = TW_STATUS & 0xF8;

        if ((status == TW_MT_SLA_ACK) || (status == TW_MR_SLA_ACK))
        {
            // Set the return value to true to indicate success
            ReturnValue = true;
        }
    }

    return ReturnValue;
}
/* End i2c_start */

/*
Function: i2c_start_wait
Description: This function will issue a start condition,
if the device is busy it will use ack polling to wait.
Parameters: Address of the slave device
Returns: Boolean (True for success)
*/
bool i2c_start_wait (const uint8_t address)
{
    uint8_t status = 0;        // Used to check the status register
    bool ReturnValue = false;  // Return Value initialised to false
    bool DeviceBusy = true;    // Set the device to busy by default

    while (DeviceBusy)
    {
        // Send the start condition to the slave
        TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

        // Wait for TWINT flag to be set. This will indicate the start condition has been transmitted
        while (!(TWCR & (1 << TWINT)));

        // Check the value of of the TWI status register
        // This 0xF8 is used to mask the prescaler bits
        status = TW_STATUS & 0xF8;
        if ((status == TW_START) || (status == TW_REP_START))
        {
            // Send the device salve address
            TWDR = address;
            TWCR = (1 << TWINT) | (1 << TWEN);

            // Wait for TWINT flag to be set
            while (!(TWCR & (1 << TWINT)));

            // Check the status register
            status = TW_STATUS & 0xF8;

            if ((status == TW_MT_SLA_ACK) || (status == TW_MR_SLA_ACK))
            {
                // Set the return value to true to indicate success
                ReturnValue = true;
                DeviceBusy = false;
            }
            else
            {
                // Release the bus
                TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);

                // Wait until stop condition has been executed
                while(TWCR & (1 << TWSTO));
            }
        }
    }

    return ReturnValue;
}
/* End i2c_start_wait*/
/*
Function: i2c_stop
Description: This function send stop condition and releases the i2c bus
Parameters: None
Returns: None
*/
void i2c_stop (void)
{
    // Transmit stop consition
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);

    // Wait for the TWSTO flag to be set
    while (!(TWCR & (1 << TWSTO)));
}
/* End i2c_stop*/

/*
Function: i2c_write
Description: This function sends 1 bytes to the i2c bus
Parameters: Byte of data to sent
Returns: Boolean (True for success)
*/
bool i2c_write (const uint8_t data)
{
    uint8_t status = 0;        // Used to check the status register
    bool ReturnValue = false;  // Return Value initialised to false

    // Transmit data to the previsouly addressed device
    TWDR = data;

    // Wait for TWINT flag to be set
    while (!(TWCR & (1 << TWINT)));

    // Check the status register
    status = TW_STATUS & 0xF8;
    if (status == TW_MT_DATA_ACK)
    {
        // Set return boolean to true to indicate success
        ReturnValue = true;
    }
    return ReturnValue;
}
/* End i2c_write */

/*
Function: i2c_read
Description: Read byte from i2c bus
Parameters: None
Returns: uint8_t of the byte read
*/
uint8_t i2c_read (void)
{
    // Set the enable i2c bus
    TWCR = (1 << TWINT) | (1 << TWEN);

    // Wait for TWINT flag to be set
    while(!(TWCR & (1 << TWINT)));

    return TWDR;
}
/* End i2c_read */
