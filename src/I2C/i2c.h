/*
I2C Dirver Library Header file
Created by: Thomas Roberts
*/

#ifndef I2C_H
#define I2C_H

#include <avr/io.h>
#include <stdbool.h>
#include <stdint.h>

#define I2C_READ  1
#define I2C_WRITE 0

extern void i2c_init (void);
extern bool i2c_start (const uint8_t address);
extern bool i2c_start_wait (const uint8_t address);
extern void i2c_stop (void);
extern bool i2c_write (const uint8_t data);
extern uint8_t i2c_read (void);

#endif
