/*
LCD Dirver Source file, this has been desgined for a 16x2 HD44780 LCD Display
connected to a PCF8574 I2C chip. The pin output of this chip to the display is
defined in the header file.
Created by: Thomas Roberts
*/
#define F_CPU 16000000UL
#include <util/delay.h>
#include "lcd.h"
#include "pcf8574.h"

uint8_t lcd_position = LCD_LINE1;

/*
Function: lcd_init
Description: This function initialises the pcf8574 chip and LCD diplay to a known state
Parameters: Data to transmit
Returns: None
Notes: 4 bit mode operation only for LCD
*/
void lcd_init (void)
{
    // Initialise the PCF8574 chip
    pcf8574_init();

    // Initialise E0 and Rs to zero
    pcf8574_setOutputPinLow(LCD_DEVICEID, LCD_E0);
    pcf8574_setOutputPinLow(LCD_DEVICEID, LCD_RS);
    _delay_ms(15);

    lcd_nibble(0x30);
    _delay_ms(4.1);
    lcd_nibble(0x30);
    _delay_us(100);
    lcd_nibble(0x30);
    _delay_us(50);
    lcd_nibble(0x20);                           // 4 bit mode
    _delay_us(50);
    lcd_command(0x28);                          // 2 lines 5*7
    lcd_command(0x08);                          // display off
    lcd_command(0x01);                          // display clear
    lcd_command(0x06);                          // cursor increment
    lcd_command(0x0C);                          // on, no cursor, no blink
}
/* End lcd_init */

/*
Function: lcd_nibble
Description: This function set the LCD data pins (on the PCF8574 chip)
to high if the equivalent bit high in the data.
Parameters: Data to transmit
Returns: None
Notes: 4 bit mode operation only for LCD
*/
void lcd_nibble (const uint8_t data)
{
    uint8_t pcf8574_data = 0;

    if (data & (1 << LCD_D4))
    {
        // Set LCD_D4 bit
        pcf8574_data = data | (1 << LCD_D4);
    }

    if (data & (1 << LCD_D5))
    {
        // Set LCD_D5 bit
        pcf8574_data = data | (1 << LCD_D5);
    }

    if (data & (1 << LCD_D6))
    {
        // Set LCD_D6 bit
        pcf8574_data = data | (1 << LCD_D6);
    }

    if (data & (1 << LCD_D7))
    {
        // Set LCD_D7 bit
        pcf8574_data = data | (1 << LCD_D7);
    }

    // Set the data to the PCF8574 chip
    pcf8574_setOutput(LCD_DEVICEID, pcf8574_data);

    // Pulse the E-Line
    pcf8574_setOutputPinHigh(LCD_DEVICEID, LCD_E0);
    _delay_us(1);
    pcf8574_setOutputPinLow(LCD_DEVICEID, LCD_E0);
}
/* End lcd_nibble */

/*
Function: lcd_byte
Description: This function will set a byte using 4 bit mode for the
lcd by calling the lcd_nibble function.
Parameters: Data to transmit
Returns: None
Notes: 4 bit mode operation only for LCD
*/
void lcd_byte (const uint8_t data)
{
    // 4 bit mode only

    // Set MSBs first
    lcd_nibble(data);
    // Set LSB Second
    lcd_nibble(data << 4);
    _delay_us(50);
}
/* End lcd_byte */

/*
Function: lcd_command
Description: This function will set command for the LCD
Parameters: Data to transmit (Command Code)
Returns: None
*/
void lcd_command (const uint8_t data)
{
    pcf8574_setOutputPinLow(LCD_DEVICEID, LCD_RS);
    lcd_byte(data);
    if (data <= 3)
    {
        // Delay needed for longer commands
        _delay_ms(3);

        // Commands below 0 reset the display set the LCD Position to line 1
        lcd_position = LCD_LINE1;
    }
    else if (data >= 0x80)
    {
        // Update lcd position
        lcd_position = data;
    }
}
/* End lcd_command */

/*
Function: lcd_putchar
Description: This function set a character to displayed on the LCD
Parameters: character to display
Returns: None
*/
void lcd_putchar (const uint8_t data)
{
    pcf8574_setOutputPinHigh(LCD_DEVICEID, LCD_RS);
    lcd_byte(data);

    // Increment Position
    lcd_position++;

    // Check to see if line is about to go off the edge
    if (lcd_position == (LCD_LINE1 + LCD_COLUMN))
    {
        // Go to line 2
        lcd_command(LCD_LINE2);
    }
    else if (lcd_position == (LCD_LINE2 + LCD_COLUMN))
    {
        // Go to line 1
        lcd_command(LCD_LINE1);
    }

}
/* End lcd_command */

/*
Function: lcd_puts
Description: This function will print a given string on the LCD display
Parameters: String to display
Returns: None
*/
void lcd_puts (const char *s)
{
    while (*s)
    {
        lcd_putchar(*s++);
    }
}
/* End lcd_puts */
