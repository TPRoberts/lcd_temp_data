/*
LCD Dirver Header file
Created by: Thomas Roberts
*/
#ifndef LCD_H
#define LCD_H

#include <stdint.h>

#define LCD_RS 0 // RS is P0 (aka Pin 4) on PCF8574
#define LCD_E0 2 // E0 is P2 (aka Pin 6) on PCF8574
#define LCD_D4 4 // D4 is P4 (aka Pin 9) on PCF8574
#define LCD_D5 5 // D5 is P5 (aka Pin 10) on PCF8574
#define LCD_D6 6 // D6 is P6 (aka Pin 11) on PCF8574
#define LCD_D7 7 // D7 is P7 (aka Pin 12) on PCF8574

#define LCD_DEVICEID 0

#define LCD_COLUMN 16
#define LCD_LINE   2
#define LCD_LINE1  0x80
#define LCD_LINE2  (0x80 + 0x40)

// Macro to set the LCD cursor to a xy position
#define lcd_xy(x, y)    lcd_command((x) + ((y==1) ? LCD_LINE2 : LCD_LINE1 ))

extern void lcd_init (void);
extern void lcd_nibble (const uint8_t data);
extern void lcd_byte (const uint8_t data);
extern void lcd_command (const uint8_t data);
extern void lcd_putchar (const uint8_t data);
extern void lcd_puts (const char *s);
#endif
