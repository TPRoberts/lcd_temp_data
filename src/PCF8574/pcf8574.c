/*
PCF8574 Dirver Source file, intended to be used with I2C driver library
Created by: Thomas Roberts
*/
#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "pcf8574.h"
#include "i2c.h"


/*
Function: pcf8574_init
Description: This function will initialise pcf8574 devices
Parameters: None
Returns: None
*/
void pcf8574_init (void)
{
    // initialise the i2c bus
    i2c_init();
    _delay_us(10);

    // Initialise the pin status
    uint8_t i = 0;
    for (i = 0; i< PCF8574_MAXDEVICES; i++)
    {
        pcf8574_PinStatus[i] = 0;
    }
}
/* End pcf8574_init */

/*
Function: pcf8574_getOutput
Description: This function will get the output status of PCF8574 device
Parameters: Device ID
Returns: Unsigned Byte giving the output pin state on PCF8574 device
*/
uint8_t pcf8574_getOutput (const uint8_t deviceId)
{
    uint8_t ReturnValue = 0xFF;

    if (deviceId < PCF8574_MAXDEVICES)
    {
        ReturnValue = pcf8574_PinStatus[deviceId];
    }
    return ReturnValue;
}
/* End pcf8574_getOutput */

/*
Function: pcf8574_getOutputPin
Description: This function will get the state of pin on a PCF8574 device
Parameters: Device ID and Pin on PCF8574
Returns: Unsigned Bytes of the sate of the pin
*/
uint8_t pcf8574_getOutputPin (const uint8_t deviceId, uint8_t pin)
{
    uint8_t ReturnValue = 0xFF;
    uint8_t TempData = 0;

    if ((deviceId < PCF8574_MAXDEVICES) && (pin < PCF8574_MAXPINS))
    {
        TempData = pcf8574_PinStatus[deviceId];
        ReturnValue = (TempData >> pin) & 0b00000001;
    }

    return ReturnValue;
}
/* End pcf8574_getOutputPin */

/*
Function: pcf8574_setOutput
Description: This function will set the output pins of a PCF8574 device
Parameters: Device ID and Data to be set
Returns: Boolean (True for success)
*/
bool pcf8574_setOutput (const uint8_t deviceId, uint8_t data)
{
    bool ReturnValue = false;

    if (deviceId < PCF8574_MAXDEVICES)
    {
        pcf8574_PinStatus[deviceId] = data;

        // Write the data to on the i2c bus
        if (i2c_start((((PCF8574_ADDRBASE + deviceId) << 1) | I2C_WRITE)))
        {
            if (i2c_write(data))
            {
                ReturnValue = true;
            }
            i2c_stop();
        }
    }
    return ReturnValue;
}
/* End pcf8574_setOutput */

/*
Function: pcf8574_setOutputPin
Description: This function will set a output pin on a PCF8574 device
Parameters: Device ID, Pin and Data
Returns: Boolean (True for success)
*/
bool pcf8574_setOutputPin (const uint8_t deviceId, const uint8_t pin, const uint8_t data)
{
    bool ReturnValue = false;
    uint8_t TempData = 0;

    if ((deviceId < PCF8574_MAXDEVICES) && (pin < PCF8574_MAXPINS))
    {
        TempData = pcf8574_PinStatus[deviceId];
        if (data != 0)
        {
            pcf8574_PinStatus[deviceId] = (TempData | (1 << pin));
        }
        else
        {
            pcf8574_PinStatus[deviceId] = (TempData & ~(1 << pin));
        }

        // Write the data to on the i2c bus
        if (i2c_start((((PCF8574_ADDRBASE + deviceId) << 1) | I2C_WRITE)))
        {
            if (i2c_write(pcf8574_PinStatus[deviceId]))
            {
                ReturnValue = true;
            }
            i2c_stop();
        }
    }

    return ReturnValue;
}
/* End pcf8574_setOutputPin */

/*
Function: pcf8574_setOutputPinHigh
Description: This function will set a specifi output pin on a PCF8574 device to high
Parameters: Device ID and Pin
Returns: Boolean (True for success)
*/
bool pcf8574_setOutputPinHigh (const uint8_t deviceId, const uint8_t pin)
{
    return pcf8574_setOutputPin(deviceId, pin, 1);
}
/* End pcf8574_setOutputPinHigh */

/*
Function: pcf8574_setOutputPinLow
Description: This function will set a specifi output pin on a PCF8574 device to low
Parameters: Device ID and Pin
Returns: Boolean (True for success)
*/
bool pcf8574_setOutputPinLow (uint8_t deviceId, uint8_t pin)
{
    return pcf8574_setOutputPin(deviceId, pin, 0);
}
/* End pcf8574_setOutputPinHigh */
