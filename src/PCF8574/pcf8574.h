/*
PCF8574 Dirver Header file
Created by: Thomas Roberts
*/

#ifndef PCF8574_H_
#define PCF8574_H_
#include <stdbool.h>
#include <stdint.h>

#define PCF8574_ADDRBASE (0x20) //device base address
#define PCF8574_MAXDEVICES 8    //max devices, depends on address (3 bit)
#define PCF8574_MAXPINS 8       //max pins per device

volatile uint8_t pcf8574_PinStatus[PCF8574_MAXDEVICES];

extern void pcf8574_init (void);
extern uint8_t pcf8574_getOutput (const uint8_t deviceId);
extern uint8_t pcf8574_getOutputPin (const uint8_t deviceId, const uint8_t pin);
extern bool pcf8574_setOutput (const uint8_t deviceId, const uint8_t data);
extern bool pcf8574_setOutputPin (const uint8_t deviceId, const uint8_t pin, const uint8_t data);
extern bool pcf8574_setOutputPinHigh (const uint8_t deviceId, const uint8_t pin);
extern bool pcf8574_setOutputPinLow (const uint8_t deviceId, const uint8_t pin);

#endif
