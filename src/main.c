/*
Main file for LCD temperature display. More information is provided in the
README
Created by: Thomas Roberts
*/
#define F_CPU 16000000UL
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "lcd.h"

int temperature = 0;
int main (void)
{
    // Setup the ADC
    // Set ADC prescalar to 128 - 125KHz with the 16MHz clock
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
    // Set ADC reference to AVCC
    ADMUX |= (1 << REFS0);
    // Left adjust ADC result to allow easy 8 bit reading
    ADMUX |= (1 << ADLAR);
     // Set ADC to auto trigger
    ADCSRA |= (1 << ADATE);
    // Enable the ADC
    ADCSRA |= (1 << ADEN);
    // Start A2D Conversions
    ADCSRA |= (1 << ADSC);
    // Enable ADC Interrupt
    ADCSRA |= (1 << ADIE);

    // init interrupt
    sei();

    // Initialise the LCD display
    lcd_init();

    // Display Buffer (2X16)
    char DispBuff[33];

    while(1)
    {
        sprintf(DispBuff, "Temperature: %d\n", temperature);
        lcd_puts(DispBuff);
        _delay_ms(500);
    }
}

// When ADC interrupt is received the ADC is complete and it is possible
// tp update the Temperature value
ISR(ADC_vect)
{
    // Calculate Temperature value
    temperature = (ADCH*(5000/1024)/10);
}
